#!/bin/bash

#Copyright 2018 Veiko Villo

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#This script should be invoked by the crontab daemon.
#An example of the crontab job:
#30 1 * * * echo -e "\n##############################################\nBackup started
#at $(date)\n##############################################" >> /var/lib/barman/pg_logs/backup.log
#&& bash /usr/local/bin/backup.sh >> /var/lib/barman/pg_logs/backup.log 2>&1

#This script will generate a list of database servers by inquiring information from the Barman server 
#and thereafter backup all the databases of the database servers on the list.

#NB! Prerequisites. Prior to activating this script be sure that:
#1) You have Barman version 2.3 installed on the managament server (Barman server).
#Information about Barman: https://www.pgbarman.org/
#2) There is backup_email.txt file in the /usr/local/bin/ directory of the Barman server. 
#Owner of the file has to be the barman user and file permissions should be 700
#3) There is backup.log file in the /var/lib/barman/pg_logs/ directory of the Barman server.
#Owner of the file has to be the barman user and file permissions should be 700

#Add your e-mail address and the name that you gave to the test server when configuring it
#on the Barman server. You can get the name by executing a command "barman list-server"
#on the Barman server as a barman OS user.
#"barman" and "test" are only examples.
#When using "barman" the e-mail is going to be sent to the barman OS user.

email_recipient="barman"
email_file="/usr/local/bin/backup_email.txt"
testServer="test"

#This function will send an e-mail to the database administrator. 
#When you invoke this function you need to pass two arguments to the function.
#The first argument is the backup_email.txt file which contains information about the backup procedure.
#The second argument is the email_recipient variable which contains e-mail address of the database administrator.
function email () {
    local content="$(cat "$1")"
    local recipient="$2"

    /usr/sbin/sendmail "$recipient" <<EOF
Subject: Report of the backup script
From: backup_script

$content

Check the /var/lib/barman/pg_logs/backup.log file on the Barman server for more information.
EOF
}

#Delete the information of the previous backup process from the e-mail file.
truncate -s 0 "$email_file"

#Start to collect information about the backup procedure and store it into the backup_email.txt file.
echo -e "*****STARTING THE BACKUP PROCEDURES at $(date)*****\n" | tee -a "$email_file"

#Generate a list of database servers. In case of failure, send an e-mail to the database administrator and exit from the script.
serverlist="$(barman list-server | awk '{print $1}')"
if [ -z "$serverlist" ]; then
    echo "Generating a list of servers for backup procedure failed at $(date). Aborting!" | tee -a "$email_file"
    email "$email_file" "$email_recipient"
    exit 1
else
    echo "A list of servers successfully generated."
fi

#Loop through all of the servers on the server list and perform the backup procedures for each of them.
while IFS= read -r server; do
    if [ "$server" = "$testServer" ]; then
        echo -e "Excluding the test server.\n" | tee -a "$email_file"
        continue
    fi

    echo "Starting to backup server $server"
    barman backup $server
    if [ $? != 0 ]; then
        echo -e "Barman command for creating backup at $(date) for server $server FAILED!\n" | tee -a "$email_file"
    else
        echo -e "Barman command for creating backup for server $server successfully executed!\n"
        echo -e "Backup for server $server at $(date) - OK!\n" | tee -a "$email_file"
    fi
done <<< "$serverlist"

echo "*****FINISHED THE BACKUP PROCEDURES at $(date)*****" | tee -a "$email_file"

#Send the information about this backup process to the database administrator.
email "$email_file" "$email_recipient"

