#!/bin/bash

#Copyright 2018 Veiko Villo

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#This script should be invoked by the crontab daemon.
#An example of the crontab job:
#30 8 * * 0 bash -x /usr/local/bin/validate_default.sh >> /var/lib/barman/pg_logs/validation.log 2>&1

#This script will enquire a list of database servers from the Barman server and then automatically test
#whether it is possible to fully restore the databases from the backups of the servers on the list.
#Backups will be restored to the remote server which only purpose is to be the testing platform.
#Successful full restore with successful logical backup (pg_dumpall) after the recovery, indicate that
#it is possible to recover the databases from this backup and the backup does not contain corrupted data.

#NB! Prerequisites. Prior to activating this script be sure that:
#1) You have Barman version 2.3 installed on the managament server (Barman server).
#Information about Barman: https://www.pgbarman.org/
#2) There is email.txt file in the /usr/local/bin/ directory of the Barman server.
#Owner of the file has to be the barman user and file permissions should be 700
#3) There is validation.log file in the /var/lib/barman/pg_logs/ directory of the Barman server.
#Owner of the file has to be the barman user and file permissions should be 700
#4) There is dumpTestRaport.txt file in the /var/lib/pgsql/ directory of the test server.
#Owner of the file has to be the postgres user and file permissions should be 700
#5) There is pg_ctl_log.txt file in the /var/lib/pgsql/ directory of the test server.
#Owner of the file has to be the postgres user and file permissions should be 700

#6) NB! If you have different versions of PostgreSQL on your server list (barman list-server) then you also have to
#have all these PostgreSQL versions installed on the test server. In order to avoid port conflict, all the PostgreSQL servers
#on the test server must be inactive! This script starts and stops different versions of PostgreSQL automatically.
#7) There is barman-wal-restore script under each PostgreSQL version's /usr/pgsql-versionNumber/bin/ directory on the test server.
#Information about the barman-wal-restore script can be found from here: https://blog.2ndquadrant.com/speed-up-getting-wal-files-from-barman/
#8) Be sure that the configuration file of the test server (for example /etc/barman.d/ssh_test.conf) on the Barman server
#contains the path_prefix that includes all the absolute paths to executable files of the different PostgreSQL's versions on the test server.
#Colon can be used to separate the absolute paths. Information about path_prefix: http://docs.pgbarman.org/release/2.3/barman.5.html   

#9) Add the IP address of your test server and Barman server. Given IP addresses are only examples.
#10) Add your e-mail address. When using "barman" the e-mail is going to be sent to the barman OS user.
#11) Add the name that you gave to the test server when configuring it on the Barman server. 
#You can get the test server's name by executing a command "barman list-server" on the Barman server.
#testServerName="test" is only an example.

pgVersion=""
stopStatus=""
exitStatus=""
serverStatus=""
endTime=""
counter=0
serverlist=""
pgServer=""
testServer="192.168.2.206"
testServerName="test"
barmanServer="192.168.2.203"
backupID=""
email_recipient="barman"
email_file="/usr/local/bin/email.txt"
nextServer=""
gotVersion=""

export barmanServer

function email () {
    local content="$(cat "$1")"
    local recipient="$2"

    /usr/sbin/sendmail "$recipient" <<EOF
Subject: Report of the recovery script
From: recovery_script

$content

Check the /var/lib/barman/pg_logs/validation.log file on the Barman server
and /var/lib/pgsql/dumpTestRaport.txt file on the test server for more information.
EOF
}

function getVersion () {
    local dbServer="$1"
    local versionNum="$(barman status $dbServer | head -5 | tail -1 | awk '{print $3}' | cut -d "." -f 1)"

    if [[ "$versionNum" = "FAILED" || -z "$versionNum" ]]; then
        gotVersion="no"
        echo "UNABLE TO retrieve the version of PostgreSQL for server $dbServer" | tee -a "$email_file"
    else
        if (( versionNum == 9 || versionNum == 10 )); then
            echo "Getting the version of PostgreSQL for server $dbServer"            
            if (( versionNum == 9 )); then
                export pgVersion="$(barman status $dbServer | head -5 | tail -1 | awk '{print $3}' | cut -d "." -f 1-2)"
                echo "PostgreSQL version is: $pgVersion"
            elif (( versionNum == 10 )); then
                export pgVersion="$(barman status $dbServer | head -5 | tail -1 | awk '{print $3}' | cut -d "." -f 1)"
                echo "PostgreSQL version is: $pgVersion"
            else
                gotVersion="no"
                echo "ERROR occurred while trying to export the version of PostgreSQL for server $dbServer" | tee -a "$email_file"
            fi
        else
            gotVersion="no"
            echo "UNKNOWN VERSION! UNABLE TO set the version of PostgreSQL for server $dbServer" | tee -a "$email_file"
        fi
    fi
}

function getBackupID () {
    local dbServer="$1"
    backupID="$(barman show-backup $dbServer latest | head -n 1 | awk '{print $2}' | cut -d ":" -f 1)"
    if [ -z "$backupID" ]; then
        echo "Failed to set variable backupID. Aborting the test procedures for server $dbServer" | tee -a "$email_file"
    else
        echo "Variable backupID set: $backupID "
        export backupID
    fi
}

function sshStopService () {
    ssh -T postgres@$testServer << EOF

    echo -e "\nYou are now logged into server \$(hostname -f) at \$(date) as \$(whoami).\n"

    /usr/pgsql-$pgVersion/bin/pg_ctl -w stop -D /var/lib/pgsql/$pgVersion/data -m smart
    if (( \$? != 0 )); then
        echo -e "Stop command for stopping the PostgreSQL service failed.\n"
    else
        echo -e "Stop command for stopping the PostgreSQL service executed successfully.\n"
    fi

    stopStatus="\$(/usr/pgsql-$pgVersion/bin/pg_ctl status -D /var/lib/pgsql/$pgVersion/data | grep server | awk '{print \$2, \$3, \$4}')"

    if [ "\$stopStatus" = "server is running" ]; then
        exit 7
    elif [ "\$stopStatus" = "no server running" ]; then
        exit 0
    else
        exit 6
    fi
EOF
}

function checkStatus () {
    local status="$1"
    local dbServer="$2"

    echo -e "\nYou are now logged into server $(hostname -f) at $(date) as $(whoami).\n"

    if (( status == 0 )); then
        echo -e "The PostgreSQL service is stopped on the test server.\n" | tee -a "$email_file" 
        serverStatus="down"
    elif (( status == 7 )); then
        echo -e "The PostgreSQL service is running on the test server.\nService has to be stopped in order to recover the backup.\nAborting recovery of backup of server $dbServer" | tee -a "$email_file"
    elif (( status == 6 )); then
        echo -e "Status of the PostgreSQL service is not verified on the test server.\nService has to be stopped in order to recover the backup.\nAborting recovery of backup of server $dbServer" | tee -a "$email_file"
    else
        echo -e "Unknown status of the PostgreSQL service on the test server.\nService has to be stopped in order to recover the backup.\nAborting recovery of backup of server $dbServer" | tee -a "$email_file"
    fi
}

function getBackupTime () {
    local backup_ID="$1"
    local dbServer="$2"

    endTime="$(barman show-backup $dbServer $backup_ID | grep "End time" | awk '{print $4, $5}')"
    if [ -z "$endTime" ]; then
        echo "Acquiring target time for recovery FAILED. Aborting recovery of backup of server $dbServer" | tee -a "$email_file"
    else
        echo -e "Target time set for recovery: $endTime"
        echo -e "Starting to recover the latest backup of server $dbServer" | tee -a "$email_file"
    fi
}

function recoverBackup () {
    local status="$1"
    local dbServer="$2"
    local end_Time="$3"
    local backup_ID="$4"

    if [ $status = "down" ]; then
        barman recover --target-time "$end_Time" $dbServer $backup_ID /var/lib/pgsql/$pgVersion/data --remote-ssh-command "ssh postgres@$testServer"
        if (( $? != 0 )); then
            echo -e "Recovery command FAILED for server $dbServer using backup $backup_ID Aborting!" | tee -a "$email_file"
            nextServer="true"
        else
            echo -e "\nRecovery command completed successfully for server $dbServer using backup $backup_ID" | tee -a "$email_file"
        fi
    else
        echo -e "The PostgreSQL service on the test server is not stopped. UNABLE TO RECOVER!\nAborting the recovery of backup $backup_ID of server $dbServer" | tee -a "$email_file"
        nextServer="true"
    fi
}

function restoreCommand () {
    ssh -T postgres@$testServer 'bash -s' << EOF

    echo -e "\nYou are now logged into server \$(hostname -f) at \$(date) as \$(whoami).\n"

    sed -i "2s|.*|restore_command = '/usr/pgsql-$pgVersion/bin/barman-wal-restore -U barman $barmanServer $pgServer %f %p'|" /var/lib/pgsql/$pgVersion/data/recovery.conf
    if grep -q "restore_command = '/usr/pgsql-$pgVersion/bin/barman-wal-restore -U barman $barmanServer $pgServer %f %p'" "/var/lib/pgsql/$pgVersion/data/recovery.conf"; then
        echo -e "Modification of the recovery file was successfull. Starting the PostgreSQL service.\n"
        /usr/pgsql-$pgVersion/bin/pg_ctl -w start -D /var/lib/pgsql/$pgVersion/data -l pg_ctl_log.txt
        if (( \$? != 0 )); then
            echo "Start command for the PostgreSQL service failed. Aborting!"
            exit 10
        else
            echo "Start command for the PostgreSQL service executed successfully."
        fi
    else
        echo -e "Modification of the recovery file failed.\n"
        exit 11
    fi
EOF
}

function doubleCheck () {
    ssh -T postgres@$testServer 'bash -s' << EOF    

    echo -e "\nChecking over the status of the PostgreSQL service on the test server after recovery and before creating the dump file."

    if [ "\$(/usr/pgsql-$pgVersion/bin/pg_ctl status -D /var/lib/pgsql/$pgVersion/data | grep server | awk '{print \$2, \$3, \$4}')" = "server is running" ]; then
        echo "Status of the PostgreSQL service on the test server after recovery: server is running."
    elif [ "\$(/usr/pgsql-$pgVersion/bin/pg_ctl status -D /var/lib/pgsql/$pgVersion/data | grep server | awk '{print \$2, \$3, \$4}')" = "no server running" ]; then
        while [[ "\$(/usr/pgsql-$pgVersion/bin/pg_ctl status -D /var/lib/pgsql/$pgVersion/data | grep server | awk '{print \$2, \$3, \$4}')" = "no server running" && "\$counter" -le 2 ]]
        do
            ((counter++))
            echo -e "\nGoing to sleep for 10 seconds because the PostgreSQL service on the test server is not active yet.\n"
            sleep 10
            echo -e "Woke up. Checking again the status of the PostgreSQL service on the test server.\n"
        done

        if (( counter == 3 )); then
            echo -e "\nThe PostgreSQL service on the test server did not start up.\n"
            exit 12
        elif (( counter < 3 )); then
            echo -e "\nThe PostgreSQL service on the test server started up. Proceeding with the recovery procedures.\n"
        else
            echo -e "\nUnknown state of the PostgreSQL service on the test server.\n"
            exit 13
        fi

    else
        echo -e "Unknown status. Unable to verify the status of the PostgreSQL service on the test server.\n"
        exit 14
    fi
EOF
}

function pgDump () {
    ssh -T postgres@$testServer 'bash -s' << EOF

    echo "#####################################################################" >> /var/lib/pgsql/dumpTestRaport.txt
    echo ""

    pg_dumpall > "/dev/null"

    if (( \$? != 0 )); then
        echo -e "Dump command failed at \$(date) for server $pgServer using backup $backupID" >> /var/lib/pgsql/dumpTestRaport.txt
        echo -e "\nDump command failed at \$(date) for server $pgServer using backup $backupID\n"
        exit 8 
    else
        echo -e "Dump command executed successfully at \$(date) for server $pgServer using backup $backupID" >> /var/lib/pgsql/dumpTestRaport.txt
        echo -e "Dump command executed successfully!\n"
        exit 9
    fi
EOF
}

function pgDumpCheck () {
    local dumpStatus="$1"

    if (( dumpStatus == 9 )); then
        echo -e "BACKUP $backupID OF SERVER $pgServer at $(date) - OK!\n" | tee -a "$email_file"
    elif (( dumpStatus == 8 )); then
        echo -e "BACKUP $backupID OF SERVER $pgServer at $(date) - ERROR!\n" | tee -a "$email_file"
    else
        echo -e "UNABLE TO VERIFY THE BACKUP STATUS OF SERVER $pgServer at $(date) - ERROR!\n" | tee -a "$email_file"
    fi
}

#Delete the information of previous recovery process from the e-mail file.
truncate -s 0 "$email_file"

if (( $? != 0 )); then
    echo "Deleting the content of the e-mail file failed."
else
    echo "Content of the e-mail file successfully deleted."
fi

echo -e "Started the recovery procedures at $(date)\n" | tee -a "$email_file" 

#Generate a list of database servers and save it into a variable.
echo "Generating a list of servers."

serverlist="$(barman list-server | awk '{print $1}')"
if [ -z "$serverlist" ]; then
    echo "Generating a list of servers failed. Aborting!" | tee -a "$email_file"
    email "$email_file" "$email_recipient"
    exit 1
else
    echo "A list of servers successfully generated."
fi

#Loop through all of the servers on the server list and do the testing procedures for each of them.
while IFS= read -r server; do
    export pgServer="$server"

    #If it is the test server then exclude it and proceed with the next server from the list.
    if [ "$server" = "$testServerName" ]; then
        echo -e "\n**************************\nExcluding the test server.\n**************************\n" | tee -a "$email_file"
        continue
    fi

    echo -e "\n*****************************************************************************************\nSTARTING THE RECOVERY PROCEDURES FOR SERVER $pgServer at $(date)\n" | tee -a "$email_file"

    #Get the version number of PostgreSQL for that server.
    getVersion "$pgServer"

    if [ "$gotVersion" = "no" ]; then
        echo -e "Proceeding with the next server from the list because script was not able to\nget the PostgreSQL version of the current server $pgServer\n" | tee -a "$email_file"
        gotVersion=""
        continue
    fi

    #Get the backup ID of the latest backup of that server.
    getBackupID "$pgServer"

    if [ -z "$backupID" ]; then
        continue
    fi

    #Open an SSH session to the test server and stop the PostgreSQL service.
    sshStopService
    #SSH session ends at this point. Workflow returns to the Barman server. 

    #Save the exit code of the function sshStopService into a variable in order to do the double check.
    exitStatus="$?"

    #Double check to make sure that the PostgreSQL service is stopped on the test server before starting the recovery.
    checkStatus "$exitStatus" "$pgServer"

    if [ -z "$serverStatus" ]; then
        continue
    fi

    #Clear the variable $exitStatus so it can be reused afterwards.
    exitStatus=""

    #Get the exact end time of the backup in order to recover the database to the time when the backup ended.
    getBackupTime "$backupID" "$pgServer"

    if [ -z "$endTime" ]; then
        continue
    fi

    #Recover the backup to the test server.
    recoverBackup "$serverStatus" "$pgServer" "$endTime" "$backupID"

    if [ ! -z "$nextServer" ]; then
        nextServer=""
        continue
    fi

    #Work flow returns to the test server.
    #Modify the restore command and start the server if the modification was successful.
    restoreCommand

    #Save the exit code of the function restoreCommand into a variable in order to add the information into the report file.
    exitStatus="$?"

    if (( $exitStatus == 0 )); then
        echo -e "\nModification of the recovery file was successfull. The PostgreSQL service on the test server has been started up.\n" | tee -a "$email_file"
    elif (( $exitStatus == 10 )); then
        echo -e "\nSTART COMMAND for the PostgreSQL service FAILED after the modification of the receovery file. Aborting!\n" | tee -a "$email_file"  
        continue
    elif (( $exitStatus == 11 )); then
        echo -e "\nModification of the recovery file failed. Aborting!\n" | tee -a "$email_file"
        continue
    else
        echo -e "\nUnknown exit code of the function restoreCommand. Aborting!\n" | tee -a "$email_file"
        continue
    fi
 
    #Clear the variable $exitStatus so it can be reused afterwards.
    exitStatus=""

    #Check the status of the PostgreSQL service on test server after recovery.
    doubleCheck

    #Save the exit code of the function doubleCheck into a variable in order to add the information into the report file.
    exitStatus="$?"

    if (( $exitStatus == 0 )); then
        echo -e "Status of the PostgreSQL service on the test server after recovery: server is running. Proceeding with a logical backup.\n" | tee -a "$email_file"
    elif (( $exitStatus == 12 )); then
        echo -e "\nThe PostgreSQL service on the test server did not start up. UNABLE TO DO A LOGICAL BACKUP! Aborting!\n" | tee -a "$email_file"
        continue
    elif (( $exitStatus == 13 )); then
        echo -e "\nUnknown state of the PostgreSQL service on the test server. UNABLE TO DO A LOGICAL BACKUP! Aborting!\n" | tee -a "$email_file"
        continue
    elif (( $exitStatus == 14 )); then
        echo -e "\nUnable to verify the status of the PostgreSQL service on the test server. UNABLE TO DO A LOGICAL BACKUP! Aborting!\n" | tee -a "$email_file"
        continue
    else
        echo -e "\nUnknown exit code of the function doubleCheck. UNABLE TO DO A LOGICAL BACKUP! Aborting!\n" | tee -a "$email_file"
        continue
    fi

    #Do a logical backup to /dev/null to test whether the backup has corrupted data.
    #Logical backup reads all of the data from the freshly restored database during the backup process,
    #so it makes sure whether the physical backup was able to restore all of the data without data corruption.
    pgDump

    #Check the exit code of the function pgDump in order to verify whether the backup is valid or not.
    pgDumpCheck "$?"

    #Stop the PostgreSQL service on the test server. Some of the different PostgreSQL versions on the test server may use the same port number.
    #In order to avoid port conflict, only one server can be active at the same time. 
    sshStopService
    #SSH sessions to the test server ends at this point.

    if (( $? == 0 )); then
        echo -e "\nThe PostgreSQL service is stopped on the test server at the end of the recovery procedures.\n" | tee -a "$email_file"  
    else
        echo -e "\nThe script was unable to stop the PostgreSQL service on the test server at the end of the recovery procedures.\n" | tee -a "$email_file"
    fi

    echo -e "END OF THE RECOVERY PROCEDURES FOR SERVER $pgServer\n*****************************************************************************************\n" | tee -a "$email_file"

    #Clear the variables before proceeding with the next server.
    stopStatus=""
    exitStatus=""
    serverStatus=""
    endTime=""
    counter=0
    backupID=""
    pgVersion=""
    nextServer=""
    gotVersion=""
done <<< "$serverlist"

echo -e "\nExited from the loop. Sending the report to the database administrator."
echo "Finished the recovery procedures at $(date)" | tee -a "$email_file"
email "$email_file" "$email_recipient"

echo -e "End of the recovery test procedures.\n\n\n\n\n\n"

